#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <signal.h>
#include <ucontext.h>

#define SIGSTKSZ 32000
#define MAX_THREADS 5
#define STACK_SIZE 1024*1024
#define NUM_THREADS 5

#define READY 1
#define TERMINATED 2
#define INVALID 3

int num_threads;

typedef struct thread
{
    int tid;
    ucontext_t context;
    char *stack;
    int finished;
    int state;
    void*(*function)(void*);
    void *arg;
    int joined;
    void *retval;
}thread_t;
ucontext_t context_scheduler;

char stack[SIGSTKSZ * (MAX_THREADS)];

thread_t threads[MAX_THREADS + 1];
thread_t *current_thread;


void sigalarm_handler(int sig)
{
    schedular();
}

void sigsegv_handler(int sig) {
    printf("Caught signal %d: Segmentation fault\n", sig);
    exit(1);
}


void wrapper(void *(*function)(void*), void *arg)
{
    void *ret = function(arg);
    thread_exit(ret);
    return;
}

int thread_create(int tid, void*(*func)(void*), void *arg)
{
    thread_t new;
    new.retval = NULL;
    new.finished = 0;
    new.joined = -1;
    new.function = func;
    new.arg = arg;
    new.stack = stack + (tid * SIGSTKSZ);
    new.tid = tid;

    new.state = READY;

    getcontext(&new.context);
    new.context.uc_stack.ss_sp = new.stack;
    new.context.uc_stack.ss_size = sizeof(new.stack);
    new.context.uc_link = &context_scheduler;
    makecontext(&new.context, (void (*)(void))wrapper, 2, new.function, new.arg);
    printf("CREATE\n");
    current_thread = &new;
    threads[num_threads++] = new;

    signal(SIGALRM, sigalarm_handler);
    ualarm(2500, 0);

    return 0;
}

int schedular(void)
{
    printf("\nSCHEDULAR\n");
    ualarm(0, 0);
    int curr;

    for(int i = 0; i <= MAX_THREADS; i++)
    {
        curr = (current_thread->tid + i) % MAX_THREADS;
        if(threads[curr].state == READY)
        {
            
            threads[curr].state = INVALID;
            swapcontext(&context_scheduler, &threads[curr].context);
            printf("baher\n");
            return 1;
        }

    }
    return 0;
}

void thread_join(int tid)
{
    ualarm(0, 0);
    
    threads[tid].joined = current_thread->tid;
    printf("\nJOIN\n");
    current_thread = &threads[tid];
    alarm(1);
    sleep(2);
    ualarm(40, 40);
}

void thread_exit(void *retval)
{
    ualarm(0, 0);
    current_thread->state = TERMINATED;
    signal(SIGSEGV, sigsegv_handler);
    current_thread->finished = 1;
    current_thread->retval = retval;

    printf("\nEXIT\n");
    ualarm(40, 40);
    schedular();
}

void *factorial(void *arg)
{
    printf("\ninside fun fact\n");
    int i=0;
    while(i<100)
    {
        printf("f");
        i++;
    }

    int fact = 1;
    return (void *)fact;

}

void *fun2(void *arg)
{
    printf("\ninsiden fun2\n");
    int i=0;
    while(i<100)
    {
        printf("2");
        i++;
    }
    int fact = 1;
    return (void *)fact;
}

void *fun3(void *arg)
{
    printf("\ninsiden fun3\n");
    int i=0;
    while(i<100)
    {
        printf("3");
        i++;
    }
    int fact = 1;
    return (void *)fact;
}

void *fun4(void *arg)
{
    printf("\ninsiden fun4\n");
    int i=0;
    while(i<100)
    {
        printf("4");
        i++;
    }
    int fact = 1;
    return (void *)fact;
}

void *fun5(void *arg)
{
    printf("\ninsiden fun5\n");
    int i=0;
    while(i<100)
    {
        printf("5");
        i++;
    }
    int fact = 1;
    return (void *)fact;
}


int main()
{
    int i;
    int arg[] = {5, 7, 3, 6, 4};
    void *retval;
    signal(SIGALRM, sigalarm_handler);
    
    thread_create(i, factorial, &arg[0]);
    thread_create(i, fun2, &arg[1]);
    thread_create(i, fun3, &arg[2]);
    thread_create(i, fun4, &arg[3]);
    thread_create(i, fun5, &arg[4]);

    for (i = 0; i < NUM_THREADS; i++)
    {
        thread_join(threads[i].tid);
    }
    return 0;

}