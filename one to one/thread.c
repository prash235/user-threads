#define _GNU_SOURCE
#include <sched.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <sys/wait.h>
#include "thread.h"

static int last_id = 0;

static void *wrapper(void *arg) {
    thread_t *thread = (thread_t *)arg;
    void *(*start_routine)(void *) = thread->start_routine;
    void *start_routine_arg = thread->arg;
    free(thread);
    start_routine(start_routine_arg);
    thread_exit(NULL);
}

int thread_create(thread_t* thread, void* (*start_routine)(void*), void* arg) {
    thread->id = ++last_id;
    thread->state = THREAD_READY;
    thread->stack_size = 1<<20; // 1 MB
    thread->stack = malloc(thread->stack_size);
    if (!thread->stack) {
        perror("malloc");
        return -1;
    }
}


int thread_create(thread_t* thread, void* (*start_routine)(void*), void* arg) {
    thread->id = ++last_id;
    thread->state = THREAD_READY;
    thread->stack_size = 1<<20; // 1 MB
    thread->stack = malloc(thread->stack_size);
    if (!thread->stack) {
        perror("malloc");
        return -1;
    }
}


int thread_join(thread_t thread, void** result){
    thread_t* current = thread_list;
    while (current != NULL) {
        if (current->id == thread.id) {
            break;
        }
        current = current->next;
    }

    if (current == NULL) {
        return -1;
    }

    while (current->execution == 0) {
        usleep(1000);
    }

    if (result != NULL) {
        *result = current->result;
    }

    return 0;
}
