#ifndef THREAD_H
#define THREAD_H

#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <ucontext.h>


typedef struct thread {
    int id;                     	/* Thread ID */
    int state;                  	/* Thread state */
    void* stack;                	/* Pointer to thread stack */
    size_t stack_size;          	/* Thread stack size */
    void* result;               	/* Thread result value */
    int execution;              	/* 1 if thread execution completes, 0 if still executing */
    int ptid;                   	/* PID of the calling process in thread structure */
    void *(*start_routine)(void*);	/* Pointer to thread function */
    void *arg;                  	/* Arguments to thread function */
    void *handle;               	/* OS-specific thread handle */
    void *(*func)(void *arg);   	/* Pointer to thread function */
    void *ret;                  	/* Return value of thread function */
    struct thread* next;        	/* Pointer to next thread in list */
} thread_t;


int thread_create(thread_t* thread, void* (*start_routine)(void*), void* arg);

int thread_join(thread_t thread, void** result);

void thread_exit(void* result);


